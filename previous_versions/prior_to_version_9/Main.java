// Copyright (c) 2024 Deltaman group limited. All rights reserved.

package com.deltaxml.merge.git.driver;

import com.deltaxml.merge.DifferentRootElementException;
import com.deltaxml.merge.DifferingOrderedAttributesException;
import com.deltaxml.merge.DoctypeChangeException;
import com.deltaxml.merge.InvalidInputException;
import com.deltaxml.merge.LicenseException;
import com.deltaxml.merge.ThreeWayMerge;
import com.deltaxml.merge.UnorderedDuplicateKeysException;
import com.deltaxml.merge.UnorderedElementContainingPCDATAException;
import net.sf.saxon.s9api.DocumentBuilder;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XPathCompiler;
import net.sf.saxon.s9api.XPathSelector;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XsltCompiler;
import net.sf.saxon.s9api.XsltTransformer;
import org.xml.sax.SAXParseException;

import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/*
 * A git merge driver implementation that uses DeltaXML merge
 */
public class Main {

  final static String CONFLICTS_REMAIN = "exists(//deltaxml:attributes) or " +
          "exists(//deltaxml:textGroup) or " +
          "exists(//*[@deltaxml:edit-type=('add', 'delete')])";

  public static void main(String[] args) {
        /*
            Parameters
            args[0] (%O) ancestor version        temp filename
            args[1] (%A) current version         temp filename
            args[2] (%B) other branches version  temp filename
            args[3] (%L) conflict marker size    positive integer (normally 7 chars)
            args[4] (%P) pathname into which the merged result will eventually be stored
         */
    ThreeWayMerge twm = new ThreeWayMerge();
    twm.setIndent(true);
    twm.setResultFormat(ThreeWayMerge.ResultFormat.XML_DELTA);
    twm.setThreeWayResultType(ThreeWayMerge.ThreeWayMergeResultType.RULE_PROCESSED_DELTAV2);
    ByteArrayOutputStream phase1outputStream = new ByteArrayOutputStream();

    try {
      // twm.setExtensionPoint(ThreeWayMerge.ExtensionPoint.INPUT_PRE_TABLE, );
      twm.merge(new FileInputStream(args[0]),
                "ancestor",
                new FileInputStream(args[1]),
                "mine",
                new FileInputStream(args[2]),
                "theirs",
                phase1outputStream);
    } catch (FileNotFoundException | LicenseException e) {
      System.out.println("DeltaXML Merge licensing problem: " + e.getMessage());
      System.exit(-3);
    } catch (InvalidInputException e) {
      SAXParseException spe = (SAXParseException) e.getCause();
      String errorMessage = String.format("Invalid XML input (SystemId: %s, line: %s, column: %s): %s", spe.getSystemId(), spe.getLineNumber(), spe.getColumnNumber(), spe.getMessage());
      System.out.println(errorMessage);
      System.exit(-1);
    } catch (UnorderedElementContainingPCDATAException |
            UnorderedDuplicateKeysException | DifferentRootElementException |
            DifferingOrderedAttributesException | DoctypeChangeException e) {
      System.out.println("XML Related processing exception:" + e.getMessage());
      System.exit(-2);
    }
    byte[] phase1Result = phase1outputStream.toByteArray();

    Processor p = new Processor(false);
    XsltCompiler xslt = p.newXsltCompiler();
    XPathCompiler xpc = p.newXPathCompiler();
    DocumentBuilder db = p.newDocumentBuilder();
    XdmNode phase1ResultXdm = null;
    boolean hasConflictsRemaining = true;
    try {
      xpc.declareNamespace("deltaxml", "http://www.deltaxml.com/ns/well-formed-delta-v1");
      XPathSelector xps = xpc.compile(CONFLICTS_REMAIN).load();
      phase1ResultXdm = db.build(new StreamSource(new ByteArrayInputStream(phase1Result)));
      xps.setContextItem(phase1ResultXdm);
      hasConflictsRemaining = xps.effectiveBooleanValue();
    } catch (SaxonApiException e) {
      System.out.println("DeltaXML Merge driver internal error in XPath processing:" + e.getMessage());
      e.printStackTrace();
      System.exit(-1);
    }
    if (hasConflictsRemaining) {
      // we have conflicts
      try {
        p.newSerializer(new File(args[4])).serializeXdmValue(phase1ResultXdm);
      } catch (SaxonApiException e) {
        e.printStackTrace();
      }
      System.exit(1);
      // we need the deltaxml markup in the result so that the interactive mergetool can
      // resolve them.  Alternatively put an automatic XSLT conflict resolver here - see
      // how XSLT is used in the else branch and customize the behaviour.
    } else {
      // no conflicts, so remove merge artifacts
      ByteArrayOutputStream phase2outputStream = new ByteArrayOutputStream();
      try {
        XsltTransformer xt = xslt.compile(new StreamSource(Main.class.getClassLoader().getResourceAsStream("clean-merge-result.xsl"))).load();
        xt.setInitialContextNode(phase1ResultXdm);
        xt.setDestination(p.newSerializer(new File(args[4])));
        xt.transform();
      } catch (SaxonApiException e) {
        e.printStackTrace();
      }
      // this should signal to git that there's no conflict resolving needed.
      System.exit(0);
    }
  }
}
