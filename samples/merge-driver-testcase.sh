#! /bin/bash
## https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
##// be careful of readlink and symlinks on macOS
SAMPLE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# clear up any previous test runs

rm -rf .git .gitattributes .gitignore
rm -f demo1.xml demo2.xml


git init
echo "*.xml merge=xmlmerge" > .gitattributes
echo "data" > .gitignore
echo "merge-driver-testcase.sh" >> .gitignore
git add .gitattributes .gitignore
git commit .gitattributes -m "initial commit of .gitattributes and .gitignore"

git config --local merge.xmlmerge.name "DeltaXML XML Merge"
git config --local merge.xmlmerge.driver "$SAMPLE_DIR/../src/main/resources/merge-driver.sh %O %A %B %L %P"

cp $SAMPLE_DIR/data/demo1/demo1-a.xml demo1.xml
cp $SAMPLE_DIR/data/demo2/demo2-a.xml demo2.xml

git add demo1.xml demo2.xml
git commit -a -m "master files initial commit"

git checkout master
git checkout -b v1

cp $SAMPLE_DIR/data/demo1/demo1-v1.xml demo1.xml
cp $SAMPLE_DIR/data/demo2/demo2-v1.xml demo2.xml

git commit -a -m "commit v1 branch file edits"

git checkout master
git checkout -b v2

cp $SAMPLE_DIR/data/demo1/demo1-v2.xml demo1.xml
cp $SAMPLE_DIR/data/demo2/demo2-v2.xml demo2.xml

git commit -a -m "commit v2 branch file edits"

git checkout master

echo ''
echo "After repository setup we have the following branches (output of: git branch --list)"
echo ''

git branch --list

## git status

##git log --graph --all
echo ''
echo ''
echo "To merge the feature branches into the master run:"
echo '   git merge v1 -m "merge v1 into master"'
echo "followed by:"
echo '   git merge v2 -m "merge v2 into master"'
