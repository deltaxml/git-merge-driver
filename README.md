# DeltaXML Git XML Merge Driver README #

This is the README for the DeltaXML git merge driver.  

It allows git to merge XML content using DeltaXML's algorithms rather then the internal line based algorithms used by git/diff3.

It depends on and is a sample for the DeltaXML-XML-Merge product which is available separately, please see [our website](
https://www.deltaxml.com/products/merge/xml-merge/?utm_source=BitBucket&utm_medium=ReadMe&utm_campaign=Git%20Merge%20Driver&utm_term=Git&utm_content=NA).

## How do I get set up? ##

### Compile and build ###

#### If you are building this with XML Merge versions prior to 9.0.0 ####

This sample has been edited and updated to work with XML Merge 9.0.0

If you are running with versions ***prior*** to XMl Merge 9.0.0 you will need to 

1) copy the file previous_versions/prior_to_version_9/Main.java over the top of src/main/java/com/deltaxml/merge/git/driver/Main.java

2) copy the file previous_versions/prior_to_version_9/pom.xml over the top of pom.xml in this directory

This is because XML Merge has updated its java API and dependencies


#### Building the driver jar ####
The XML-Merge release provides some jar files that are not available in a public maven repository, but
which are included in our XML-Merge release. 

In order to install these as local (`$HOME/.m2`) dependencies you must run the POM first with the initialize phase.

In order for this to run correctly the POM must be told which version of XML Merge you are using. These properties are defined at the top of the `pom.xml`.  The pom file can be edited or these properties can be specified whilst building.  

The easiest way of building the sample is to clone or unpack this code into the samples subdirectory of the XML-Merge release, for example:

```
pwd
/Users/nigelw/Downloads/DeltaXML-XML-Merge-9_0_0_j/samples
git clone https://bitbucket.org/deltaxml/git-merge-driver.git
```

In this example the version of XML Merge downloaded is 9.0.0 which uses XML Compare 11.0.0, but you may have earlier or later versions.
In the downloaded directory you can find the deltaxml jars and the versions which you need to specify.

deltaxml-X.Y.Z.jar provides the xml.compare.version, X.Y.Z

deltaxml-merge-A.B.C.jar provides the xml.merge.version, A.B.C

```
/usr/local/deltaxml/DeltaXML-XML-Merge-9_0_0_j $ ls -l deltaxml-*.jar
-rw-r--r--  1 nigelw  deltaxml   2287956 21 Jun 12:37 deltaxml-11.0.0.jar
-rw-r--r--  1 nigelw  deltaxml   1281199 21 Jun 12:37 deltaxml-merge-9.0.0.jar
-rw-r--r--  1 nigelw  deltaxml  57172248 21 Jun 12:37 deltaxml-merge-rest-9.0.0.jar
-rw-r--r--  1 nigelw  deltaxml   8327427 21 Jun 12:37 deltaxml-merge-rest-client-9.0.0.jar
```

We can specify these versions on the Maven command line as follows

  * `mvn initialize -Dxml.merge.version=9.0.0 -Dxml.compare.version=11.0.0`

If you clone or unpack somewhere other than the samples subdirectory it will also be necessary to specify an install.dir property either on the command-line or in `pom.xml`:  

  * `mvn initialize -Dxml.merge.version=9.0.0 -Dxml.compare.version=11.0.0 -Dxml.merge.install.dir=/usr/local/deltaxml/DeltaXML-XML-Merge-9_0_0_j`
  
After that one off step you can build the driver with `mvn package` - you still need to specify the versions if you did not edit the POM to set them up;
  
  * `mvn package -Dxml.merge.version=9.0.0 -Dxml.compare.version=11.0.0`
  
  

### License setup ###

If you are evaluating DeltaXML-Merge or have purchased a subcription you will have a license file
with the filename: `deltaxml-merge.lic`.  A copy of this file should be place in the
directory containing merge-driver jar file.  The maven build should copy this file
to the `target` directory, but if you customize the build or make a permanent installation
you may also need to copy/install the license file.

### Sample data and script ###

The `samples` directory contains sample data and a shell script to create a repository
structure as a demonstration of the merge driver.

Running the script will create a repository with three branches: master, v1 and v2.  The final lines
of the script suggest git commands that should be used merge the v1 and v2 branches back onto master.
The script will run in-situ after the compile and build stage above.  Here is an example:

```
$ ./merge-driver-testcase.sh 
Initialized empty Git repository in /Users/nigelw/dev/git-merge-driver/samples/.git/
[master (root-commit) e843bca] initial commit of .gitattributes and .gitignore
 1 file changed, 1 insertion(+)
 create mode 100644 .gitattributes
[master 08238fa] master files initial commit
 3 files changed, 18 insertions(+)
 create mode 100644 .gitignore
 create mode 100644 demo1.xml
 create mode 100644 demo2.xml
Already on 'master'
Switched to a new branch 'v1'
[v1 294bb57] commit v1 branch file edits
 2 files changed, 1 insertion(+), 5 deletions(-)
Switched to branch 'master'
Switched to a new branch 'v2'
[v2 a9872c6] commit v2 branch file edits
 2 files changed, 2 insertions(+), 1 deletion(-)
Switched to branch 'master'

After repository setup we have the following branches (output of: git branch --list)

* master
  v1
  v2

To merge the feature branches into the master run:
   git merge v1 -m "merge v1 into master"
followed by:
   git merge v2 -m "merge v2 into master"

$ git merge v1 -m "merge v1 into master"
Updating 08238fa..294bb57
Fast-forward (no commit created; -m option ignored)
 demo1.xml | 2 +-
 demo2.xml | 4 ----
 2 files changed, 1 insertion(+), 5 deletions(-)

$ git merge v2 -m "merge v2 into master"
DeltaXML XML Merge Driver: conflicts remain and need resolving for demo2.xml
DeltaXML XML Merge Driver: conflicts remain and need resolving for demo1.xml
Auto-merging demo2.xml
CONFLICT (content): Merge conflict in demo2.xml
Auto-merging demo1.xml
CONFLICT (content): Merge conflict in demo1.xml
Automatic merge failed; fix conflicts and then commit the result.
```

Note: If you move the script or the driver jar file to more permanent locations then the new
locations should be used to update the sample script and/or driver script.

### Merge Driver Permanent Setup ###

While the sample above has been configured with an easy test/demonstration of a merge, for other use
cases we would recommend a more permanent and flexible install using absolute file paths.

Copy the provided `src/main/resources/merge-driver.sh` file to a permanent location.

Create `.gitattributes` with patterns in your git repository to associate xml files with the merge drivers.  For example:
```
*.xml    merge=xmlmerge
pom.xml  merge=xmlmerge
```

Then in git config configure the xml merge driver, using --local, --global or --system as appropriate:
```
$ git config --local merge.xmlmerge.name "DeltaXML XML Merge"
$ git config --local merge.xmlmerge.driver "/Users/nigelw/bin/merge-driver.sh %O %A %B %L %P"

```

The `merge driver.sh` script invokes java with the jar file built using maven.  The path
used in the script is best changed to an absolute path on your filesystem.
 

## Future work ##

This driver will use our algorithms to merge structured/tree-based content and represent conflicts in a structured/processable way.
Some users will be able to process our change/conflict representation using, for example, XSLT or an XML editor and others may want an GUI based conflict resolving tool to be used as a 'git mergetool'.
We hope to provide samples of both automatic and also GUI based conflict resolving soon. 

Please get in touch if you have specific use cases you would like us to consider.

## Feedback ##

Please use our [support portal](https://docs.deltaxml.com/support/latest/support-channels-13304648.html?utm_source=BitBucket&utm_medium=ReadMe&utm_campaign=Git%20Merge%20Driver&utm_term=Git&utm_content=NA) (Jira Service Desk).


Pull requests for enhancement or fixes are also welcome via bitbucket.

## About DeltaXML ##

We provide flawless XML and JSON comparison and merge solutions to identify, visualise and implement XML and JSON diffs quickly and accurately.
Our products are available as Java API, RESTful API and SaaS, to learn more please [visit our website](
https://www.deltaxml.com/products/?utm_source=BitBucket&utm_medium=ReadMe&utm_campaign=Git%20Merge%20Driver&utm_term=Git&utm_content=NA).

