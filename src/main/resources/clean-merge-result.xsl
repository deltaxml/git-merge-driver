<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    exclude-result-prefixes="deltaxml"
    xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
    version="3.0">

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="@* | node()">
      <xsl:copy copy-namespaces="false">
        <xsl:apply-templates select="@*, node()"/>
      </xsl:copy>
    </xsl:template>

    <xsl:template match="@deltaxml:*"/>

</xsl:stylesheet>